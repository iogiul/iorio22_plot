#include <iostream>
#include <math.h>
#include <chrono>
#include <vector>
#include <random>
#include <algorithm>
#include <fstream>
#include <iomanip>

constexpr double G = 3.925125598496094e8; //RSUN^3 YR^-2 MSUN^-1
constexpr double c = 1.3598865132357053e7; // RSun/yr
constexpr double c5_over_G3 = (c*c*c*c*c)/(G*G*G); //Scaling for GW processes


/**
 * GW merging time scale from Peters64
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @return time scale in yr
 */
double Tpeters(double M1, double M2, double a, double e){

    double e2=e*e;
    double scaling = 5./256.*c5_over_G3;
    double fe = 1+73./24.*e2 + 37./96.*e2*e2;

    return scaling*(a*a*a*a)*std::pow(1-e2,3.5)/(fe * (M1*M2*(M1+M2)) );
}

/**
 * Modified GW merging time scale
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @return time scale in yr
 */
double Tgw(double M1, double M2, double a, double e){

    double scaling = 5./256.*c5_over_G3;

    return scaling*(a*a*a*a)*std::pow(1-e*e,3.5) / (M1*M2*(M1+M2));
}

/**
 * Fit to difference between Tgw and Tmerge as estimated in Iorio+22
 * @param e eccentricity
 * @return correction factor fc(e) (Tmerge= Tgw/(1+fc(e))
 */
double Ioriofit(double e){

    double e2=e*e;
    double exponent = 1.105 -0.807*e +0.193*e2;
    double base = (1-std::pow(e,3.074));

    return e2*(-0.443 + 0.580*std::pow(base,exponent));

}

/**
 * Analytic equation to approximate Tmerge starting from Tgw (Iorio+22)
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @return merging time in yr
 */
double TIorio(double M1, double M2, double a, double e){

    double GWtime = Tgw(M1,M2,a,e);
    double fcorr  = Ioriofit(e);

    return GWtime/(1+fcorr);
}

/**
 * Analytic equation to approximate Tmerge starting from Tpeters (Zwick+20)
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @return merging time in yr
 */
double TZwick(double M1, double M2, double a, double e){

    double GWtime = Tpeters(M1,M2,a,e);
    double fcorr  = std::pow(8,1-std::sqrt(1-e));

    return GWtime*fcorr;
}

double Tcombined(double M1, double M2, double a, double e){

    if (e>0.999) return TZwick(M1,M2,a,e);

    return TIorio(M1,M2,a,e);
}

/**
 * Analytic equation to approximate Tmerge starting from TGW (Mandel+20, https://arxiv.org/pdf/2110.09254.pdf)
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @return merging time in yr
 */
double TMandel(double M1, double M2, double a, double e){

    double GWtime = Tgw(M1,M2,a,e);

    double fe10=std::pow(e,10);
    double fe20=std::pow(fe10,2);
    double fe1000=std::pow(fe20,50);
    double fcorr = 1 + 0.27*fe10 + 0.33*fe20 + 0.2*fe1000;

    return GWtime*fcorr;
}


/****************** ODE *************************/

/**
 * Systems of ode for the GW orbital decay by Peters64
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @param dadt Time derivative of a [Rsun/yr]
 * @param dedt  Time derivative of e [1/yr]
 */
void dfdt_Peters(double M1, double M2, double a, double e, double& dadt, double& dedt){

    if (e<0) e=0.;
    if (a<0) a=1e-20;

    static double M1_c=0.;
    static double M2_c=0.;
    static double cost=0.;

    //Caching
    if (M1_c!=M1 or M2_c!=M2){
        M1_c=M1;
        M2_c=M2;
        cost = M1_c*M2_c*(M1_c+M2_c)/c5_over_G3;
    }

    double a3=a*a*a;
    double a4=a3*a;
    double e2=e*e;
    double e4= e2*e2;

    dadt = -64./5. * cost / (a3 * std::pow(1-e2,3.5) ) * (1. +73./24.*e2 + 37./96.*e4); //Rsun/yr
    dedt = -304./15. * cost * e / (a4 * std::pow(1-e2,2.5)) * (1. +121./304.*e2); //Rsun/yr

    return;
}

/********* Euler **************/
/**
 * A single evolution step using the Euler method
 * @param a Initial value of semimajor axis [Rsun]
 * @param e Initial value of the eccentricity
 * @param fa Time derivative of a [Rsun/yr]
 * @param fe Time derivative of e [1/yr]
 * @param h timestep [yr]
 * @param anew final value of the semimajor axis [Rsun]
 * @param enew final value of the eccentricity
 */
void Euler_step(double a, double e, double fa, double fe, double h, double& anew, double& enew){
    anew = a + fa*h;
    enew = e + fe*h;
}

/**
 * Adaptive method to evolve the Peters ODE with the Euler method and find the merging time
 * The evolution is stopped when a is smaller than the last stable orbit radius of the most massive object (6M/c^2)
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @return Merging time in yr
 */
double Tadaptive_euler(double M1, double M2, double a, double e){

    double rlo = 6.*G*std::max(M1,M2)/(c*c); //Rsun

    double toll=1e-2;
    double h_adaptive_increase=2.;
    double h_adaptive_decrease=10.;

    double h = 3.17098e-8; //In seconds
    double fa=0., fe=0.,t=0.;
    double aold=a,eold=e;

    while (aold>=rlo){
        dfdt_Peters(M1,M2,a,e,fa,fe);
        Euler_step(aold,eold,fa,fe,h,a,e);

        //Check if too slow
        if (std::abs(a-aold)/aold<0.1*toll){
            h=h*h_adaptive_increase;
            Euler_step(aold,eold,fa,fe,h,a,e);
        }

        //Check if too fast
        while(std::abs(a-aold)/aold>toll){
            h=h/h_adaptive_decrease;
            Euler_step(aold,eold,fa,fe,h,a,e);
        }

        //std::cout<<t<<" "<<a<<" "<<e<<" "<<fa<<" "<<fe<<" "<<h<<std::endl;
        //Here a and e are updated
        t=t+h;
        aold=a;
        eold=e;
    }


    return t;


}

/********* Runge Kutta 4 *******/
/**
 * A single evolution step using the Runke Kutta 4th order method
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a Initial value of semimajor axis [Rsun]
 * @param e Initial value of the eccentricity
 * @param fa Time derivative of a [Rsun/yr]
 * @param fe Time derivative of e [1/yr]
 * @param h timestep [yr]
 * @param anew final value of the semimajor axis [Rsun]
 * @param enew final value of the eccentricity
 */
void Runge4_step(double M1, double M2, double a, double e, double fa, double fe, double h, double& anew, double& enew){

    double ka[4] = {0,0,0,0};
    double ke[4] = {0,0,0,0};

    ka[0]=h*fa;
    ke[0]=h*fe;

    dfdt_Peters(M1,M2,a+0.5*ka[0],e+0.5*ke[0],fa,fe);
    ka[1]=h*fa;
    ke[1]=h*fe;

    dfdt_Peters(M1,M2,a+0.5*ka[1],e+0.5*ke[1],fa,fe);
    ka[2]=h*fa;
    ke[2]=h*fe;

    dfdt_Peters(M1,M2,a+ka[2],e+ke[2],fa,fe);
    ka[3]=h*fa;
    ke[3]=h*fe;

    anew = a + (1./6.) * (ka[0] + 2*ka[1] + 2*ka[2] + ka[3]);
    enew = e + (1./6.) * (ke[0] + 2*ke[1] + 2*ke[2] + ke[3]);
}

/**
 * Adaptive method to evolve the Peters ODE with Runge Kutta 4th order method and find the merging time
 * The evolution is stopped when a is smaller than the last stable orbit radius of the most massive object (6M/c^2)
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @return Merging time in yr
 */
double Tadaptive_runge4(double M1, double M2, double a, double e){

    double rlo = 6.*G*std::max(M1,M2)/(c*c); //Rsun

    double toll=1e-2;
    double h_adaptive_increase=2.;
    double h_adaptive_decrease=10.;

    double h = 3.17098e-8; //In seconds
    double fa=0., fe=0.,t=0.;
    double aold=a,eold=e;

    while (aold>=rlo){
        dfdt_Peters(M1,M2,a,e,fa,fe);
        Runge4_step(M1,M2,aold,eold,fa,fe,h,a,e);

        //Check if too slow
        if (std::abs(a-aold)/aold<0.1*toll){
            h=h*h_adaptive_increase;
            Runge4_step(M1,M2,aold,eold,fa,fe,h,a,e);
        }

        //Check if too fast
        while(std::abs(a-aold)/aold>toll){
            h=h/h_adaptive_decrease;
            Runge4_step(M1,M2,aold,eold,fa,fe,h,a,e);
        }

        //Here a and e are updated
        t=t+h;
        aold=a;
        eold=e;
    }


    return t;


}


/************************ Utilites  ************************/

/**
 * Generate a random set of bynary properties (Masses,semimajor axis, eccentricity)
 * The masses are generated from a uniform distribution in the range 0.5-300 Msun,
 * the semimajor axis are drawn from an uniform log distribution between 0.1-10^10 Rsun
 * the eccentricity distribution is uniform between 0 and 1
 * @param N  Number of system to generate
 * @param seed random seed
 * @return a vector of vector of doubles, the outer most vector has dimension N, the innermost has dimension 4:
 *      - v1: Mass_1 in Msun
 *      - v2: Mass_2 in Msun
 *      - a: Semimajor axis in Rsun
 *      - e: eccentricity
 */
std::vector<std::vector<double>> generate_sample(int N=100, int seed=42){

    std::vector<std::vector<double>> vec;
    vec.resize(N);

    std::mt19937 rgen(seed);
    std::uniform_real_distribution<double> udist(0.0,1.0);

    for (auto& v : vec){
        v.push_back(udist(rgen)*(300-0.5)+0.5); //M1
        v.push_back(udist(rgen)*(300-0.5)+0.5); //M2
        v.push_back(std::pow(10,udist(rgen)*(10-1)+1)); //a
        if (udist(rgen)>0.8) v.push_back(udist(rgen)*(0.99-0.9)+0.9);
        else v.push_back(udist(rgen)); //e
    }

    return vec;
}

/**
 * Function to time the T estimate
 * @param M1 Mass of the first object [Msun]
 * @param M2 Mass of the second object [Msun]
 * @param a  Semimajor axis [Rsun]
 * @param e  Eccentricity
 * @param func Pointer to the  function used to estimate the T double (*func)(double M1,double M2,double a,double e)
 * @param label String to use in the printing
 * @param N number of repetitions
 */
void test_func(double M1, double M2, double a, double e,
               double (*func)(double,double,double,double ),std::string label, int N=1000){

    std::chrono::steady_clock::time_point clock_start, clock_end;
    clock_start = std::chrono::steady_clock::now();
    double T;
    for (int i=0; i<N; i++){
        T = func(M1,M2,a,e);
    }
    clock_end = std::chrono::steady_clock::now();
    long Total_time = std::chrono::duration_cast<std::chrono::microseconds>(clock_end - clock_start).count();

    std::cout << label << " " << T << " yr; time (s): " << Total_time/(N*1e6) << std::endl;

}

/**
 * Function to estimate the computation time of a given function applied to a list of input
 * @param input a vector of vector of doubles, the outer most vector has dimension N, the innermost has dimension 4:
 *      - v1: Mass_1 in Msun
 *      - v2: Mass_2 in Msun
 *      - a: Semimajor axis in Rsun
 *      - e: eccentricity
 * @param func Pointer to the  function used to estimate the T double (*func)(double M1,double M2,double a,double e)
 * @param label String to use in the printing
 * @return  a vector of double containing the estimated merger time for each input
 */
std::vector<double>  test_func(const std::vector<std::vector<double>>& input,double (*func)(double,double,double,double ),std::string label){

    std::vector<double> results;

    std::chrono::steady_clock::time_point clock_start, clock_end;
    clock_start = std::chrono::steady_clock::now();
    for (auto& v : input){
        results.push_back(func(v[0],v[1],v[2],v[3]));
    }
    clock_end = std::chrono::steady_clock::now();
    long Total_time = std::chrono::duration_cast<std::chrono::nanoseconds>(clock_end - clock_start).count();

    std::cout << label << " time (s): " << Total_time/(input.size()*1e9) << std::endl;

    return results;
}

/**
 * Compare the relative elemntwise difference between two vectors:
 * rdiff=abs(res1-restrue)/restrue
 * The function prints in output the maximum and the average relative difference
 * @param res1 Vector of double
 * @param restrue  Vector of double
 * @param label String to use in the printing
 */
void compare_results(const std::vector<double>& res1, const std::vector<double>& restrue, std::string label){

    double max_diff=-1.0;
    double rel_diff=0.;
    double accum_rel_diff=0.;
    double count=0;

    for (int i=0; i<res1.size(); i++){
        if (restrue.at(i)>0){
            rel_diff = std::abs(res1.at(i)-restrue.at(i))/restrue.at(i);
            accum_rel_diff+=rel_diff;
            max_diff = std::max(max_diff,rel_diff);
            count++;
        }
    }

    std::cout << label << " Maximum relative difference " << max_diff << " Average diff " << accum_rel_diff/count << std::endl;
}

/**
* Read the input from a csv file
*/
std::vector<std::vector<double>> read_input(std::string filename){

    std::string st;
    std::ifstream finput;
    finput.open(filename);

    if (!finput){
        std::cerr<<"Cannot open file"<<std::endl;
    }

    std::getline(finput, st);

    std::vector<std::vector<double>> v;
    constexpr int Ncol=26;
    int row_counter=0;
    int counter=0;
    while (std::getline(finput, st,',')) {

        //NOTICE: getline return also the \n character, moreover
        // the last columns do not terminate with , therefore getline get the last_column + \n character plus the first column of the new row
        // To quickly fix this behaviour (we don't nee neither the first of the las column) we assume that the actual number of column if N-1


        if(counter%(Ncol-1)==0){
            v.push_back({0.,0.,0.,0.});
        }

        // Add to the list of output strings
        switch(counter%(Ncol-1)){
            case 0:
                if (counter>0) row_counter++;
                break;
            case 3:
                v[row_counter][0]=std::stod(st);
                break;
            case 8:
                v[row_counter][1]=std::stod(st);
                break;
            case 13:
                v[row_counter][2]=std::stod(st);
                break;
            case 14:
                v[row_counter][3]=std::stod(st);
                break;
            default:
                break;
        }

        //std::cout<<" C "<< st << "  A " << counter<<" L "<<counter%(Ncol)<<" B "<<row_counter<<std::endl;
        //for (auto vvv : v[row_counter]){
        //  std::cout<< " "<< vvv <<std::endl;
        //}


        counter++;
    }
    v.pop_back(); //remove last row that is empty
    finput.close();


    return v;

}



int main() {


    std::vector<std::vector<double>> input =read_input("../tdelay_fit_test_dataset.csv");



    std::cout<<" N to integrate "<< input.size();
    std::cout<<" "<<std::endl;

    auto tpet= test_func(input, &Tpeters, "Tpeters");
    auto tgw = test_func(input, &Tgw, "Tgw");
    auto tiorio = test_func(input, &TIorio, "TIorio");
    auto tzwick = test_func(input, &TZwick, "TZwick");
    auto tcomb = test_func(input, &Tcombined, "Tcombined");
    auto tmandel = test_func(input, &TMandel, "TMandel");
    auto teuler = test_func(input, &Tadaptive_euler, "TEuler");
    auto trk4 = test_func(input, &Tadaptive_runge4, "TRK4");


    std::cout<<" "<<std::endl;

    compare_results(tpet,trk4,"Tpeters");
    compare_results(tgw,trk4,"Tgw");
    compare_results(tiorio,trk4,"TIorio");
    compare_results(tzwick,trk4,"TZwick");
    compare_results(tcomb,trk4,"Tcombined");
    compare_results(tmandel,trk4,"TMandel");
    compare_results(teuler,trk4,"TEuler");


    //Save system
    std::ofstream foutput;
    foutput.open("Tmerge_results.csv");
    foutput<<"M1,M2,a,e,Trk4,Teuler,Tpeters,Tgw,Tiorio,Tzwick,Tcombined,TMandel"<<std::endl;
    for (int i=0; i<input.size();i++){
        for(auto &vi : input[i]){
            foutput<<vi<<",";
        }
        foutput<<trk4[i]<<","<<teuler[i]<<","<<tpet[i]
        <<","<<tgw[i]<<","<<tiorio[i]<<","<<tzwick[i]<<","<<tcomb[i]<<","
        <<tmandel[i]<<std::endl;
    }
    foutput.close();


    return EXIT_SUCCESS;
}
