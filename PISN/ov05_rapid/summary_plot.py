import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import re
from matplotlib.colors import LogNorm, PowerNorm, Normalize
import matplotlib.colors as colors
import matplotlib.cm as cmx
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import matplotlib as mpl
label_size = 15
mpl.rcParams['xtick.labelsize'] = label_size
mpl.rcParams['ytick.labelsize'] = label_size
#mpl.rcParams['mathtext.default']='regular'
mpl.rcParams.update({'figure.autolayout': True})
mpl.rcParams['contour.negative_linestyle'] = 'solid'
mpl.rcParams['axes.facecolor'] = 'white'
mpl.rcParams['xtick.major.size'] = 6
mpl.rcParams['xtick.major.width'] = 1.5
mpl.rcParams['xtick.minor.size'] = 3
mpl.rcParams['xtick.minor.width'] = 1
mpl.rcParams['ytick.major.size'] = 6
mpl.rcParams['ytick.major.width'] = 1.5
mpl.rcParams['ytick.minor.size'] = 3
mpl.rcParams['ytick.minor.width'] = 1

def SN_info(logfile):
    """
    Read a lofgile filtering the SN formation event and getting their preSN properties
    @param logfile: path to the logfile to read
    @return: a pandas dataframe with the following columns
        - name: name of system the exploding star belongs to  (or just name of the star for SSE)
        - SID: ID of the star inside the binary (0 for primary, 1 for secondary)
        - Mass_presn: preSN mass of the star
        - MHE_presn: preSN He-core mass of the star
        - MCO_presn: preSN CO-core mass of the star
        - Mass_remnant: mass of the remnant object after the SN
        - Type: type of remnant
    """

    matchnum="[0-9]+\.?[0-9]*e?[+|-]?[0-9]*|(?i)nan"
    matchname="(?:[0-9|A-Za-z]*\_)?[0-9]*"
    matchid="[0-9]+"
    matchtype="[+|-]?\d+"
    matchexpr=f"S;({matchname});{matchid};SN;(?:{matchnum});({matchnum}):({matchnum}):({matchnum}):({matchnum}):({matchtype})"

    with open(logfile,"r") as fo:
        ma = re.findall(matchexpr,fo.read())

    na = np.array(ma)

    df = pd.DataFrame(na,columns=["name","Mass_presn","MHE_presn","MCO_presn","Mass_remnant","Type"])

    #In some rare cases, in the same timestep of a supernova explosion the companion call a repeat in this case
    #two SN events can appear for the same stare in the logfile. The last one is the right one.
    #With the following command we remove the duplicates
    df = df.drop_duplicates(subset=("name"), keep="last")

    return df.astype({"name":"int64",
                     "Mass_presn":"float",
                     "MHE_presn":"float",
                     "MCO_presn":"float",
                     "Mass_remnant":"float",
                     "Type":"int"})



def create_final_file(outfolder: str) -> pd.DataFrame:

    out = pd.read_csv(f"{outfolder}/output_0.csv")

    outf = out[out.Worldtime>0][["name","Mass"]].rename(columns={"Mass":"Mrem"})
    outi = out[out.Worldtime==0][["name","Zams","Zmet"]].rename(columns={"Zams":"Mzams"})

    outf = outf.merge(outi, on="name")
    dflog=SN_info(f"{outfolder}/logfile_0.dat")[["name","Mass_presn","MHE_presn","MCO_presn","Type"]]
    outf = outf.merge(dflog, on="name")

    return outf

if __name__=="__main__":

    Zs=(0.0001,0.0005,0.001,0.005,0.01,0.02)


    jet = cm = plt.get_cmap('plasma')
    cNorm  = LogNorm(vmin=np.min(Zs), vmax=np.max(Zs))
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)


    folders=("sevn_output_mapelli20","sevn_output_farmer19")
    fig = plt.figure(figsize=(12,8))

    gs = fig.add_gridspec(2, 2, hspace=0)

    for i,folder in enumerate(folders):
        print(folder)
        dfo=create_final_file(folder)
        ax=fig.add_subplot(gs[0,i])
        plt.sca(ax)
        ax.axes.xaxis.set_ticklabels([])
        for j,Z in enumerate(Zs):
            ls="solid" if j%2==0 else "dashed"
            dft=dfo[dfo.Zmet==Z]
            colorVal = scalarMap.to_rgba(Z)
            plt.plot(dft.Mzams,dft.Mrem,color=colorVal,label=f"$Z$={Z}",lw=2,ls=ls)
            ax.xaxis.set_major_locator(MultipleLocator(25))
            ax.xaxis.set_minor_locator(MultipleLocator(5))
            ax.yaxis.set_major_locator(MultipleLocator(20))
            ax.yaxis.set_minor_locator(MultipleLocator(5))
            ax.tick_params(axis="y", which="both", direction="in",right="on")
            ax.tick_params(axis="x", which="both", direction="in",right="on",top="on")
            ax.set_xlabel("Mzams [M$_\odot$]")
            if i==0:
                ax.set_ylabel("$M_\mathrm{rem}$ [M$_\odot$]",fontsize=label_size)
                plt.text(10,101,"PISN model: M20",fontsize=label_size)
            elif i==1:
                ax.set_ylabel("$M_\mathrm{rem}$ [M$_\odot$]",fontsize=label_size)
                plt.text(10,101,"PISN model: F19",fontsize=label_size)
            elif i==2:
                ax.set_ylabel("$M_\mathrm{rem}$ [M$_\odot$]",fontsize=label_size)
                plt.text(10,90,"PISN: Iorio+22  \n (Farmer+19)",fontsize=label_size)


        plt.xlim(0,200)
        plt.ylim(0,110)
        plt.grid(which="both",lw=0.1)
        if i==1: plt.legend(loc="upper right")

        if i==1:
            ax1=fig.add_subplot(gs[i,0])
            ax2=fig.add_subplot(gs[i,1])
            for j,Z in enumerate(Zs):
                ls="solid" if j%2==0 else "dashed"
                dft=dfo[dfo.Zmet==Z]
                colorVal = scalarMap.to_rgba(Z)
                plt.sca(ax1)
                plt.plot(dft.Mzams,dft.MHE_presn,color=colorVal,label=f"{Z}",lw=2,ls=ls)
                plt.axhline(32,ls="dashed",c="gray")
                plt.text(10,25,"32 M$_\odot$",fontsize=label_size-2)
                plt.axhline(37,ls="dashed",c="gray")
                plt.text(10,38.5,"37 M$_\odot$",fontsize=label_size-2)
                plt.axhline(64,ls="dashed",c="gray")
                plt.text(10,65.5,"64 M$_\odot$",fontsize=label_size-2)
                plt.axhline(135,ls="dashed",c="gray")
                ax1.xaxis.set_major_locator(MultipleLocator(25))
                ax1.xaxis.set_minor_locator(MultipleLocator(5))
                ax1.yaxis.set_major_locator(MultipleLocator(20))
                ax1.yaxis.set_minor_locator(MultipleLocator(5))
                plt.xlim(0,200)
                plt.ylim(0,110)
                ax1.set_xlabel("$M_\mathrm{ZAMS}$ [M$_\odot$]",fontsize=label_size)
                ax1.set_ylabel("$M_\mathrm{He}$ preSN [M$_\odot$]",fontsize=label_size)
                #plt.text(10,101,"$M_\mathrm{He}$ preSN",fontsize=label_size)
                plt.sca(ax2)
                plt.plot(dft.Mzams,dft.MCO_presn,color=colorVal,label=f"{Z}",lw=2,ls=ls)
                plt.axhline(38,ls="dashed",c="gray")
                plt.text(10,39.5,"38 M$_\odot$",fontsize=label_size-2)
                plt.axhline(60,ls="dashed",c="gray")
                plt.text(10,61.5,"60 M$_\odot$",fontsize=label_size-2)
                ax2.xaxis.set_major_locator(MultipleLocator(25))
                ax2.xaxis.set_minor_locator(MultipleLocator(5))
                ax2.yaxis.set_major_locator(MultipleLocator(20))
                ax2.yaxis.set_minor_locator(MultipleLocator(5))
                ax1.tick_params(axis="y", which="both", direction="in",right="on")
                ax1.tick_params(axis="x", which="both", direction="in",right="on",top="on")
                ax2.tick_params(axis="y", which="both", direction="in",right="on")
                ax2.tick_params(axis="x", which="both", direction="in",right="on",top="on")
                plt.xlim(0,200)
                plt.ylim(0,110)
                ax2.set_xlabel("$M_\mathrm{ZAMS}$ [M$_\odot$]",fontsize=label_size)
                ax2.set_ylabel("$M_\mathrm{CO}$ preSN [M$_\odot$]",fontsize=label_size)
                #plt.text(10,101,"$M_\mathrm{CO}$ preSN",fontsize=label_size)

            ax1.grid(which="both",lw=0.1)
            ax2.grid(which="both",lw=0.1)

    """
    dfo=create_final_file("sevn_output_iorio22_limited")
    ax=fig.add_subplot(gs[1,2])
    plt.sca(ax)
    for j,Z in enumerate(Zs):
        ls="solid" if j%2==0 else "dashed"
        dft=dfo[dfo.Zmet==Z]
        colorVal = scalarMap.to_rgba(Z)
        plt.plot(dft.Mzams,dft.Mrem,color=colorVal,label=f"{Z}",lw=2,ls=ls)
        ax.xaxis.set_major_locator(MultipleLocator(25))
        ax.xaxis.set_minor_locator(MultipleLocator(5))
        ax.yaxis.set_major_locator(MultipleLocator(20))
        ax.yaxis.set_minor_locator(MultipleLocator(5))
        ax.tick_params(axis="y", which="both", direction="in",right="on")
        ax.tick_params(axis="x", which="both", direction="in",right="on",top="on")
        plt.xlim(0,200)
        plt.ylim(0,110)
    ax.set_xlabel("$M_\mathrm{ZAMS}$ [M$_\odot$]",fontsize=label_size)
    ax.set_ylabel("$M_\mathrm{rem}$  [M$_\odot$]",fontsize=label_size)
    plt.text(10,94,"PISN: Iorio+22  limited \n (Farmer+19)",fontsize=label_size)
    plt.grid(which="both",lw=0.1)
    """

    fig.tight_layout()
    fig.savefig("Mrem_pisn_ov05.pdf")


    exit()
